package com.sda.simpletodolist2.api;

import com.sda.simpletodolist2.core.dto.AddTaskForm;
import com.sda.simpletodolist2.core.model.TaskEntity;
import com.sda.simpletodolist2.core.service.TaskService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping("/com/sda/simpletodolist2/api")
@AllArgsConstructor
public class ListRestApiController {

    @Autowired
    private TaskService taskService;

    @GetMapping("/tasks")
    public ResponseEntity all () {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ResponseEntity.ok(taskService.getAllTasks());
//                new ResponseEntity<List<TaskDTO>>(taskService.getAllTasks(), HttpStatus.OK) ;
    }

    @PostMapping("/tasks")
    public ResponseEntity createTask (@RequestBody AddTaskForm addTaskForm,
                               @RequestParam String email)
    {
        TaskEntity task = taskService.createTask(addTaskForm, email);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(task.getId())
                .toUri();
        return ResponseEntity.created(uri).body(task);
    }
//    @Autowired

//    private final UserService userService;
//    @RequestMapping(path = "users")
//    public List<UserEntity> allUsers() {
//        return

//    }


}
