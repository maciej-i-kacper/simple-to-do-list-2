package com.sda.simpletodolist2.core.controller;


import com.sda.simpletodolist2.core.dto.ChangePasswordForm;
import com.sda.simpletodolist2.core.exception.UserNotFoundException;
import com.sda.simpletodolist2.core.service.UserService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping
public class ChangePasswordController {

    private final UserService userService;
    private final Validator validator;

    public ChangePasswordController(UserService userService, @Qualifier("changePasswordValidator") Validator validator) {
        this.userService = userService;
        this.validator = validator;
    }

    @InitBinder
    void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    @GetMapping(value = "/changePassword")
    ModelAndView getChangePasswordPage() {
        ModelAndView mnv = new ModelAndView("changePassword");
        mnv.addObject("changePasswordForm", new ChangePasswordForm());
        return mnv;
    }

    @PostMapping(value = "/newPassword")
    String setNewPassword(@ModelAttribute @Validated ChangePasswordForm changePasswordForm, BindingResult bindingResult) throws UserNotFoundException {
        if (bindingResult.hasErrors()) {
            return "changePassword";
        }
        userService.changePassword(changePasswordForm);
        return "redirect:/login";
    }


}
