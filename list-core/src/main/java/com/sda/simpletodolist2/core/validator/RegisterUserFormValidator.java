package com.sda.simpletodolist2.core.validator;


import com.sda.simpletodolist2.core.dto.RegisterUserForm;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class RegisterUserFormValidator implements Validator {


    @Override
    public boolean supports(Class<?> aClass) {
        return RegisterUserForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        RegisterUserForm form = (RegisterUserForm) o;

        if (StringUtils.isBlank(form.getEmail())) {
            errors.rejectValue("email", "validator.field.notEmpty");
        }
        if (StringUtils.isBlank(form.getName())) {
            errors.rejectValue("name", "validator.field.notEmpty");
        }
        if (StringUtils.isBlank(form.getSurname())) {
            errors.rejectValue("surname", "validator.field.notEmpty");
        }
        if (StringUtils.isBlank(form.getPassword())) {
            errors.rejectValue("password", "validator.field.notEmpty");
        }
        if (StringUtils.isBlank(form.getRepeatedPassword())) {
            errors.rejectValue("repeatedPassword", "validator.field.notEmpty");
        }
        if (!form.getPassword().equals(form.getRepeatedPassword())) {
            errors.rejectValue("password", "registerUserForm.validator.password.notEquals");
        }
        if (StringUtils.isBlank(form.getControlQuestion())) {
            errors.rejectValue("controlQuestion", "validator.field.notEmpty");
        }
        if (StringUtils.isBlank(form.getAnswer())) {
            errors.rejectValue("answer", "validator.field.notEmpty");
        }
    }
}
