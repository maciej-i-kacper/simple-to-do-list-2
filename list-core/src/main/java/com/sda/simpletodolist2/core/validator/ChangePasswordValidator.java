package com.sda.simpletodolist2.core.validator;

import com.sda.simpletodolist2.core.dto.ChangePasswordForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ChangePasswordValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return ChangePasswordForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        ChangePasswordForm form = (ChangePasswordForm) o;

    }
}
