package com.sda.simpletodolist2.core.controller;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Collection;

@Controller
public class HomeController {

    @GetMapping("/")
    public String getAdminOrUserHomePage() {
        Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        for (GrantedAuthority authority : authorities) {
            String authString = authority.getAuthority();
            String adminString = "ROLE_ADMIN";
            if (authString.equals(adminString)) {
                return "redirect:/users";
            }
        }
        return "redirect:/tasks";
    }
}
