package com.sda.simpletodolist2.core.controller;

import com.sda.simpletodolist2.core.dto.RegisterUserForm;
import com.sda.simpletodolist2.core.exception.UserExistsException;
import com.sda.simpletodolist2.core.service.UserService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping
public class RegisterUserController {

    private final UserService userService;
    private final Validator validator;

    public RegisterUserController(UserService userService, @Qualifier("registerUserFormValidator") Validator validator) {
        this.userService = userService;
        this.validator = validator;
    }

    @InitBinder
    void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    @GetMapping(value = "/register")
    ModelAndView getRegisterPage() {
        ModelAndView mnv = new ModelAndView("register");
        mnv.addObject("registerUserForm", new RegisterUserForm());
        return mnv;
    }

    @PostMapping(value = "/register")
    public String createTask(@ModelAttribute(name = "registerUserForm") RegisterUserForm registerUserForm,
                             BindingResult bindingResult) throws UserExistsException {
        if (bindingResult.hasErrors()) {
            return "register";
        }
        userService.createUser(registerUserForm);
        return "redirect:/login";
    }

    @PostMapping(value = "/addUser")
    String addUserAction(@ModelAttribute @Validated RegisterUserForm registerUserForm, BindingResult bindingResult) throws UserExistsException {
        if (bindingResult.hasErrors()) {
            return "register";
        }
        RegisterUserForm form = registerUserForm;
        userService.createUser(form);
        return "redirect:/login";
    }

}
