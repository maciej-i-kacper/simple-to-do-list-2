package com.sda.simpletodolist2.core.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class TaskDTO {

    private long id;
    private String title;
    private String description;
    private LocalDateTime creationDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private LocalDateTime deadline;
    private boolean isDone;

}
