package com.sda.simpletodolist2.core.controller;

import com.sda.simpletodolist2.core.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@AllArgsConstructor
public class AdminController {

    private final UserService userService;

    @GetMapping("/users")
    public ModelAndView getAllUsers() {
        ModelAndView mnv = new ModelAndView("users");
        mnv.addObject("users", userService.getAllUsers());
        return mnv;
    }
}
