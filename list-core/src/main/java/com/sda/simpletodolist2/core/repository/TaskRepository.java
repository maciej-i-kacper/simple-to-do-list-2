package com.sda.simpletodolist2.core.repository;

import com.sda.simpletodolist2.core.model.TaskEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TaskRepository extends JpaRepository<TaskEntity, Long> {
    List<TaskEntity> findByUserEntityEmail(String email);

}
