package com.sda.simpletodolist2.core.service;

import com.sda.simpletodolist2.core.dto.ChangePasswordForm;
import com.sda.simpletodolist2.core.dto.RegisterUserForm;
import com.sda.simpletodolist2.core.exception.UserExistsException;
import com.sda.simpletodolist2.core.exception.UserNotFoundException;
import com.sda.simpletodolist2.core.model.UserEntity;
import com.sda.simpletodolist2.core.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
//@Transactional
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public void createUser(RegisterUserForm registerUserForm) throws UserExistsException {
        if (userRepository.existsByEmail(registerUserForm.getEmail())) {
            throw new UserExistsException("User with that name exists");
        }
        String encodedPassword = passwordEncoder.encode(registerUserForm.getPassword());
        UserEntity userEntity = new UserEntity(
                registerUserForm.getName(),
                registerUserForm.getSurname(),
                registerUserForm.getDateOfBirth(),
                registerUserForm.getEmail(),
                encodedPassword,
                registerUserForm.getControlQuestion(),
                registerUserForm.getAnswer());
        if (userEntity.getName().equals("admin")) {
            userEntity.setAdmin(true);
//                UserRoleEntity admin = userRoleEntityRepository.findByName("ADMIN");
//                userEntity.getRoles().add(admin);
        }
        userRepository.save(userEntity);
    }

    @Override
    public UserEntity getUserByEmail(String email) throws UserNotFoundException {
        UserEntity userEntity = userRepository.findByEmail(email).orElse(null);
        if (userEntity == null) {
            throw new UserNotFoundException("User not found");
        } else {
            return userEntity;
        }
    }


    @Override
    public void changePassword(ChangePasswordForm changePasswordForm) throws UserNotFoundException {
        UserEntity user = getUserByEmail(changePasswordForm.getEmail());
        user.setPassword(passwordEncoder.encode(changePasswordForm.getPassword()));
        user.setControlQuestion(changePasswordForm.getControlQuestion());
        user.setAnswer(changePasswordForm.getAnswer());
        userRepository.save(user);
    }

    @Override
    public List<UserEntity> getAllUsers() {
        return userRepository.findAll();
    }


}
