package com.sda.simpletodolist2.core.service;

import com.sda.simpletodolist2.core.dto.AddTaskForm;
import com.sda.simpletodolist2.core.dto.TaskDTO;
import com.sda.simpletodolist2.core.model.TaskEntity;
import com.sda.simpletodolist2.core.model.UserEntity;

import java.util.List;

public interface TaskService {

    TaskEntity createTask(AddTaskForm addTaskForm, String email);

    List<TaskDTO> getAllTasks();

    List<TaskDTO> getTaskByEmail(String email);
}
