package com.sda.simpletodolist2.core.mapper;

import com.sda.simpletodolist2.core.dto.TaskDTO;
import com.sda.simpletodolist2.core.dto.UserDTO;
import com.sda.simpletodolist2.core.model.TaskEntity;
import com.sda.simpletodolist2.core.model.UserEntity;

public class ModelMapper {

    private ModelMapper() {

    }

    public static UserDTO mapUser(UserEntity userEntity) {
        return new UserDTO(userEntity.getId(),
                userEntity.getName(),
                userEntity.getSurname(),
                userEntity.getDateOfBirth(),
                userEntity.getEmail(),
                userEntity.getPassword(),
                userEntity.getControlQuestion(),
                userEntity.getAnswer(),
                userEntity.isAdmin());
    }

    public static TaskDTO mapTask(TaskEntity taskEntity) {
        return new TaskDTO(taskEntity.getId(),
                taskEntity.getTitle(),
                taskEntity.getDescription(),
                taskEntity.getCreationDate(),
                taskEntity.getDeadline(),
                taskEntity.isDone());
    }


}
