package com.sda.simpletodolist2.core.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@NoArgsConstructor
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String surname;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateOfBirth;
    private String email;
    private String password;
    private String controlQuestion;
    private String answer;
    private boolean isAdmin = false;

//    @OneToMany
//    @JoinColumn(name = "id_user")
//    private List<TaskEntity> tasks = new ArrayList<>();

    public UserEntity(String name, String surname, LocalDate dateOfBirth, String email, String password, String controlQuestion, String answer) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.email = email;
        this.password = password;
        this.controlQuestion = controlQuestion;
        this.answer = answer;
        this.isAdmin = false;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setControlQuestion(String controlQuestion) {
        this.controlQuestion = controlQuestion;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    //    public void addToTaskList(TaskEntity taskEntity) {
//        tasks.add(taskEntity);
//    }

    //            (mappedBy = "user", fetch = FetchType.LAZY)
}
