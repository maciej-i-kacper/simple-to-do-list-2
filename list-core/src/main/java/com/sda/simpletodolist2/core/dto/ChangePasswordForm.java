package com.sda.simpletodolist2.core.dto;

import lombok.Data;

@Data
public class ChangePasswordForm {

    private String email;
    private String password;
    private String repeatedPassword;
    private String controlQuestion;
    private String answer;

}
