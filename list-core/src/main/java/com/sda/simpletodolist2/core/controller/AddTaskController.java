package com.sda.simpletodolist2.core.controller;


import com.sda.simpletodolist2.core.dto.AddTaskForm;
import com.sda.simpletodolist2.core.service.TaskService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AddTaskController {

    private final TaskService taskService;

    public AddTaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping(value = "/add")
    String addTask(AddTaskForm addTaskForm){
        String email = SecurityContextHolder.getContext().getAuthentication().getName();
        taskService.createTask(addTaskForm,email);
        return "redirect:/tasks";
    }

}
