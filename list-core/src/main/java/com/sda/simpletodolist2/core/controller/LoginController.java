package com.sda.simpletodolist2.core.controller;


import com.sda.simpletodolist2.core.dto.LoginForm;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/login")
public class LoginController {

    @RequestMapping
    ModelAndView getLoginPage(){
        ModelAndView modelAndView = new ModelAndView("login");
        modelAndView.addObject("loginForm", new LoginForm());
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return modelAndView;
    }

}
