package com.sda.simpletodolist2.core.mock;

import com.sda.simpletodolist2.core.dto.AddTaskForm;
import com.sda.simpletodolist2.core.dto.RegisterUserForm;
import com.sda.simpletodolist2.core.exception.UserExistsException;
import com.sda.simpletodolist2.core.service.TaskService;
import com.sda.simpletodolist2.core.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Service
@AllArgsConstructor
public class MockData {

//    UserRepository userRepository = new User

    private UserService userService;
    private TaskService taskService;
//    = new UserServiceImpl()

    @PostConstruct
    public void mockData() throws UserExistsException {

        RegisterUserForm registerUserForm = new RegisterUserForm();

        registerUserForm.setName("Kacper");
        registerUserForm.setSurname("Czub");
        registerUserForm.setEmail("kacper.czub@wp.pl");
        registerUserForm.setDateOfBirth(LocalDate.of(1995, 9, 19));
        registerUserForm.setPassword("dupa");
        registerUserForm.setRepeatedPassword("dupa");
        registerUserForm.setControlQuestion("dupa");
        registerUserForm.setAnswer("dupa");

        userService.createUser(registerUserForm);

        RegisterUserForm registerAdminForm = new RegisterUserForm();

        registerAdminForm.setName("admin");
        registerAdminForm.setSurname("admin");
        registerAdminForm.setEmail("admin.czub@wp.pl");
        registerAdminForm.setDateOfBirth(LocalDate.of(1995, 9, 19));
        registerAdminForm.setPassword("dupa");
        registerAdminForm.setRepeatedPassword("dupa");
        registerAdminForm.setControlQuestion("dupa");
        registerAdminForm.setAnswer("dupa");

        userService.createUser(registerAdminForm);


        AddTaskForm addUserTaskForm = AddTaskForm
                .builder()
                .title("Uruchomić zajebistą aplikację springową")
                .deadline(LocalDateTime.of(2019, 11, 24, 4, 00))
                .description("dupa dupa")
                .build();

        taskService.createTask(addUserTaskForm, "kacper.czub@wp.pl");
        taskService.createTask(addUserTaskForm, "admin.czub@wp.pl");


    }
}
