package com.sda.simpletodolist2.core.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor
public class TaskEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String title;
    private String description;
    private LocalDateTime creationDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private LocalDateTime deadline;
    private boolean isDone;

//    @OneToMany(mappedBy = "")
//    @JoinColumn(name = "id_")

//    @ManyToOne(mappedBy = "id_user", fetch = FetchType.LAZY)
    @ManyToOne
    private UserEntity userEntity;

    public TaskEntity(String title, String description, LocalDateTime deadline, UserEntity userEntity) {
        this.title = title;
        this.description = description;
        this.creationDate = LocalDateTime.now();
        this.deadline = deadline;
        this.isDone=false;
        this.userEntity=userEntity;
    }


}
