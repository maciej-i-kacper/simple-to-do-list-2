package com.sda.simpletodolist2.core.dto;

import lombok.Data;

@Data
public class LoginForm {
    private String email;
    private String password;


}
