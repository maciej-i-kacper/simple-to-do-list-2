package com.sda.simpletodolist2.core.service;

import com.sda.simpletodolist2.core.model.UserEntity;
import com.sda.simpletodolist2.core.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userEmail) throws UsernameNotFoundException {
        Optional<UserEntity> byUserName = userRepository.findByEmail(userEmail);
        UserEntity userEntity = byUserName.orElseThrow(() -> new UsernameNotFoundException("User not found"));
        if (userEntity.isAdmin()) {
            return new User(userEntity.getEmail(), userEntity.getPassword(), adminGrantedAuthorities());
        }
        return new User(userEntity.getEmail(), userEntity.getPassword(), userGrantedAuthorities());
    }

    private List<GrantedAuthority> adminGrantedAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        return authorities;
    }
    private List<GrantedAuthority> userGrantedAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        return authorities;
    }
}
