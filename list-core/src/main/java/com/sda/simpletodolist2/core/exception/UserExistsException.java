package com.sda.simpletodolist2.core.exception;

public class UserExistsException extends Exception {

    public UserExistsException(String message) {
        super(message);
    }
}
