package com.sda.simpletodolist2.core.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
//@Order(2)
@EnableWebSecurity
public class SpringSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    @Qualifier("userDetailsServiceImpl")
    private UserDetailsService userDetailsService;
    private static final String[] MATCHERS = {"/", "api/tasks", "/add", "/api/tasks/**", "/register", "/tasks/**", "/tasks", "/h2/**", "/webjars/**"};
    private static final String[] ADMIN_MATCHERS = {"/admin/**", "/users"};

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .headers().frameOptions().disable()
                .and()
                .authorizeRequests()
                .antMatchers(ADMIN_MATCHERS).hasRole("ADMIN")
                .antMatchers(MATCHERS).hasRole("USER")
                .antMatchers("/register", "/changePassword", "/h2/**").permitAll()
                .antMatchers("/login").permitAll()
                .anyRequest().authenticated()
                //.authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .defaultSuccessUrl("/");
//                .usernameParameter("email")
//                .passwordParameter("password")
//                .permitAll();
        /*http.csrf().disable()
                .headers().frameOptions().disable()
                .and()
                .authorizeRequests()
                .antMatchers(MATCHERS)
                .permitAll()
                .antMatchers(ADMIN_MATCHERS)
                .hasRole("ADMIN")
                .and()
                .formLogin()
                .loginPage("/login")
                .usernameParameter("email")
                .passwordParameter("password")*/

        //.and()
        //.logout()
        //.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
        //.logoutSuccessUrl("/login")
        // .permitAll();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Bean
    static PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}