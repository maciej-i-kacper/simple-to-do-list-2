package com.sda.simpletodolist2.core.service;

import com.sda.simpletodolist2.core.dto.ChangePasswordForm;
import com.sda.simpletodolist2.core.dto.RegisterUserForm;
import com.sda.simpletodolist2.core.exception.UserExistsException;
import com.sda.simpletodolist2.core.exception.UserNotFoundException;
import com.sda.simpletodolist2.core.model.UserEntity;

import java.util.List;

public interface UserService {

    void createUser(RegisterUserForm registerUserForm) throws UserExistsException;

    UserEntity getUserByEmail(String email) throws UserNotFoundException;

    void changePassword(ChangePasswordForm changePasswordForm) throws UserNotFoundException;

    List<UserEntity> getAllUsers();
}
