package com.sda.simpletodolist2.core.service;

import com.sda.simpletodolist2.core.dto.AddTaskForm;
import com.sda.simpletodolist2.core.dto.TaskDTO;
import com.sda.simpletodolist2.core.mapper.ModelMapper;
import com.sda.simpletodolist2.core.model.TaskEntity;
import com.sda.simpletodolist2.core.repository.TaskRepository;
import com.sda.simpletodolist2.core.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Transactional
public class TaskServiceImpl implements TaskService {

    UserRepository userRepository;
    TaskRepository taskRepository;

    @Override
    public TaskEntity createTask(AddTaskForm addTaskForm, String email) {
        TaskEntity taskEntity = new TaskEntity(addTaskForm.getTitle(), addTaskForm.getDescription(), addTaskForm.getDeadline(),
                userRepository.findByEmail(email).orElse(null));
//        UserEntity userEntity = userRepository.findByEmail(email).orElse(null);
//        userEntity.getTasks().add(taskEntity);
//        userEntity.addToTaskList(taskEntity);

        taskRepository.save(taskEntity);
        return taskEntity;
    }

    @Override
    public List<TaskDTO> getAllTasks() {
        return taskRepository
                .findAll()
                .stream()
                .map(ModelMapper::mapTask)
                .collect(Collectors.toList());
    }

//    public List<TaskDTO> getTa

    @Override
    public List<TaskDTO> getTaskByEmail(String email) {
        return taskRepository.findByUserEntityEmail(email)
                .stream()
                .map(ModelMapper::mapTask)
                .collect(Collectors.toList());
    }
}
