package com.sda.simpletodolist2.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleToDoList2CoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleToDoList2CoreApplication.class, args);
    }

}
