package com.sda.simpletodolist2.core.controller;


import com.sda.simpletodolist2.core.service.TaskService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TaskController {

    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping("/tasks")
    ModelAndView getTasks() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String email = SecurityContextHolder.getContext().getAuthentication().getName();
        ModelAndView mnv = new ModelAndView("tasks");
        mnv.addObject("tasks", taskService.getTaskByEmail(email));
        return mnv;
    }

}
